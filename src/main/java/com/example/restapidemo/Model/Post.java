package com.example.restapidemo.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Post {
    int id;
    String title;
    String des;
    String sub;
    String file;
    int vote;
    String createAt;
}
