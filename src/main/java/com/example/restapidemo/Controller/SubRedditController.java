package com.example.restapidemo.Controller;

import com.example.restapidemo.Model.SubReddit;
import com.example.restapidemo.Service.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class SubRedditController {

    @Autowired
    SubredditService subredditService;


    @GetMapping("/subreddits/create")
    public String subreddit() {
        return "PostReddit";
    }

    @PostMapping("/addReddit")
    public String create(@ModelAttribute SubReddit subReddit) {
        String title = subReddit.getTitle();
        String des = subReddit.getDes();
        subredditService.insert(title, des);
        return "redirect:/";
    }
}
