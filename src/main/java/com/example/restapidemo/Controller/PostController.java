
package com.example.restapidemo.Controller;
import com.example.restapidemo.Model.Post;
import com.example.restapidemo.Model.SubReddit;
import com.example.restapidemo.Service.PostService;
import com.example.restapidemo.Service.SubredditService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.AttributedString;
import java.util.Date;


@Controller
public class PostController {

    @Autowired
    PostService postService;

    @Autowired
    SubredditService subredditService;



    @GetMapping("/")
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("sub", subredditService.getAllSub());
        modelMap.addAttribute("post", postService.getAllPost());
        return "index";
    }

    @GetMapping("/posts/create")
    public String post(ModelMap modelMap) {
        modelMap.addAttribute("sublist", subredditService.getAllSub());
        return "Post";
    }

    @PostMapping("/addPost")
    public String create(@ModelAttribute Post post) {
        String title = post.getTitle();
        String des = post.getDes();
        String sub = post.getSub();
        postService.insert(title, des, sub);
        return "redirect:/";
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        postService.delete(id);
        return "redirect:/";
    }

    @RequestMapping("/update/{id}")
    public String getPostbyID(ModelMap modelMap,@PathVariable("id") int id){
        modelMap.addAttribute("post",  postService.getPostbyID(id));
        return "PostUpdate";
    }

    @PostMapping("/updatePost/{id}")
    public String update(@ModelAttribute Post post, @PathVariable("id") int id){
        String title = post.getTitle();
        String des = post.getDes();

        postService.update(id, title, des);
        return "redirect:/";
    }

}