package com.example.restapidemo.Service;


import com.example.restapidemo.Model.Post;
import com.example.restapidemo.Model.SubReddit;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PostService {
    String insert (String title, String des, String sub);
    List<Post> getAllPost();
    String delete (int id);
    String update(int id, String title, String des);
    Post getPostbyID(int id);
}
