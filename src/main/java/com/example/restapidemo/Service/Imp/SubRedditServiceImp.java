package com.example.restapidemo.Service.Imp;

import com.example.restapidemo.Model.SubReddit;
import com.example.restapidemo.Repository.PostRepository;
import com.example.restapidemo.Repository.SubRedditRepository;
import com.example.restapidemo.Service.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubRedditServiceImp implements SubredditService {

    @Autowired
    private SubRedditRepository subRedditRepository;

    @Override
    public String insert(String title, String des) {
         subRedditRepository.insert(title, des);
         return null;
    }

    @Override
    public List<SubReddit> getAllSub() {
        List<SubReddit> sub = subRedditRepository.findAll();
        return sub;
    }
}
