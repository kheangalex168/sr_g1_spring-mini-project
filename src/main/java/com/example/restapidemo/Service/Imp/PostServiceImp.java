package com.example.restapidemo.Service.Imp;

import com.example.restapidemo.Model.Post;
import com.example.restapidemo.Model.SubReddit;
import com.example.restapidemo.Repository.PostRepository;
import com.example.restapidemo.Repository.SubRedditRepository;
import com.example.restapidemo.Service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImp implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Override
    public String insert(String title, String des, String sub) {
        postRepository.insert(title, des, sub);
        return null;
    }

    @Override
    public List<Post> getAllPost() {
        List<Post> post = postRepository.findAll();
        return post;
    }

    public String delete(int id){
        postRepository.delete(id);
        return null;
    }

    public Post getPostbyID(int id){
       Post post = postRepository.getPostbyID(id);
        return post;
    }

    public String update(int id, String title, String des){
        postRepository.update(id, title, des);
        return null;
}
}
