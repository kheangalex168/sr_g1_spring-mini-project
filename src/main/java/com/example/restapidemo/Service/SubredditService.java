package com.example.restapidemo.Service;

import com.example.restapidemo.Model.SubReddit;

import java.util.List;

public interface SubredditService {
    String insert (String title, String des);
    List<SubReddit> getAllSub();
}
