package com.example.restapidemo.Repository;

import com.example.restapidemo.Model.Post;
import com.example.restapidemo.Model.SubReddit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PostRepository {

    @Select("insert into posts(title,des,sub)  values(#{title},#{des},#{sub})")
    String insert (String title, String des, String sub);


    @Select("select * from posts")
    List<Post> findAll();


    @Select("delete from posts where id=#{id}")
    String delete (int id);

    @Select("select * from posts where id=#{id}")
    Post getPostbyID(int id);

    @Select("UPDATE posts SET title = #{title}, des = #{des} WHERE id=#{id}")
    String update(int id, String title, String des);
}
