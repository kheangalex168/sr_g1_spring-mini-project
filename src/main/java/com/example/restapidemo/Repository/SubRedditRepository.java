package com.example.restapidemo.Repository;

import com.example.restapidemo.Model.SubReddit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SubRedditRepository {
    @Select("insert into subreddits(title,des)  values(#{title},#{des})")
    String insert (String title, String des);


    @Select("select * from subreddits")
    List<SubReddit> findAll();

}
